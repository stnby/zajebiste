#include <iostream>
#include <strings.h>

using namespace std;

struct specialist {
	string name;
	specialist *next;
};

struct specialty {
	string title;
	specialist *specialists;
	int amount;
	specialty *next;
};

struct company {
	string name;
	specialty *specialties;
	company *next;
};

specialty *firstSpecialty = NULL;
company *firstCompany = NULL;

specialty *registerSpecialty() {
	specialty *newSpecialty = new specialty;
	cout << "Įrašykite specialybės pavadinimą:\n";
	cin.ignore();
	getline(cin, newSpecialty->title);
	specialty *currSpecialty = firstSpecialty;
	while (currSpecialty != NULL) {
		if (currSpecialty->title == newSpecialty->title) {
			delete newSpecialty;
			return currSpecialty;
		}
		currSpecialty = currSpecialty->next;
	}
	newSpecialty->next = firstSpecialty;
	firstSpecialty = newSpecialty;
	return newSpecialty;
}

void registerSpecialist() {
	string specialtyTitle;
	specialty *currSpecialty = firstSpecialty;
	while (currSpecialty != NULL) {
		cout << currSpecialty->title << endl;
		currSpecialty = currSpecialty->next;
	}
	cout << "Įrašykite specialybės pavadinimą:\n";
	cin.ignore();
	getline(cin, specialtyTitle);
	currSpecialty = firstSpecialty;
	while (currSpecialty != NULL) {
		if (currSpecialty->title == specialtyTitle) {
			specialist *newSpecialist = new specialist;
			specialist *currSpecialist = currSpecialty->specialists;
			specialist *prevSpecialist;
			cout << "Įveskite specialisto pavardę:\n";
			cin.ignore();
			getline(cin, newSpecialist->name);
			while (currSpecialist != NULL && newSpecialist->name > currSpecialist->name) {
				prevSpecialist = currSpecialist;
				currSpecialist = currSpecialist->next;
			}
			if (prevSpecialist == NULL)
				currSpecialty->specialists = newSpecialist;
			else
				prevSpecialist->next = newSpecialist;
			newSpecialist->next = currSpecialist;
			return;
		}
		currSpecialty = currSpecialty->next;
	}
	cout << "Ši specialybė neegzistuoja.\n";
}

void listEverything() {
	specialty *currSpecialty = firstSpecialty;
	while (currSpecialty != NULL) {
		cout << currSpecialty->title << endl;
		specialist *currSpecialist = currSpecialty->specialists;
		while (currSpecialist != NULL) {
			cout << "    " << currSpecialist->name << endl;
			currSpecialist = currSpecialist->next;
		}
		currSpecialty = currSpecialty->next;
	}
}

void registerCompany() {
	company *newCompany = new company;
	cout << "Įrašykite įmonės pavadinimą:\n";
	cin.ignore();
	getline(cin, newCompany->name);
	company *currCompany = firstCompany;
	while (currCompany != NULL) {
		if (currCompany->name == newCompany->name) {
			cout << "Ši įmonė jau yra užregistruota.\n";
			delete newCompany;
			return;
		}
		currCompany = currCompany->next;
	}
	newCompany->next = firstCompany;
	firstCompany = newCompany;
}

void requestSpecialists() {
	string companyName;
	cout << "Įrašykite įmonės pavadinimą:\n";
	cin.ignore();
	getline(cin, companyName);
	company *currCompany = firstCompany;
	while (currCompany != NULL) {
		if (currCompany->name == companyName) {
			specialty *currSpecialty = registerSpecialty();
			specialty *newSpecialty = new specialty;
			newSpecialty->title = currSpecialty->title;
			cout << "Įveskite kiekį: ";
			cin >> newSpecialty->amount;
			newSpecialty->next = currCompany->specialties;
			currCompany->specialties = newSpecialty;
			return;
		}
		currCompany = currCompany->next;
	}
	cout << "Ši įmonė neegzistuoja.\n";
}

void listCompanyRequirements() {
	string companyName;
	cout << "Įrašykite įmonės pavadinimą:\n";
	cin.ignore();
	getline(cin, companyName);
	company *currCompany = firstCompany;
	while (currCompany != NULL) {
		if (currCompany->name == companyName) {
			specialty *currSpecialty = currCompany->specialties;
			while (currSpecialty != NULL) {
				cout << currSpecialty->amount << "vnt. " << currSpecialty->title << endl;
				currSpecialty = currSpecialty->next;
			}
			return;
		}
		currCompany = currCompany->next;
	}
	cout << "Ši įmonė neegzistuoja.\n";
}

void provideSpecialists() {
	string companyName;
	cout << "Įrašykite įmonės pavadinimą:\n";
	cin.ignore();
	getline(cin, companyName);
	company *currCompany = firstCompany;
	company *prevCompany;
	while (currCompany != NULL) {
		prevCompany = currCompany;
		if (currCompany->name == companyName) {
			specialty *currSpecialty = currCompany->specialties;
			specialty *prevSpecialty;
			while (currSpecialty != NULL) {
				prevSpecialty = currSpecialty;
				specialty *currSpecialty2 = firstSpecialty;
				while (currSpecialty2 != NULL) {
					if (currSpecialty2->title == currSpecialty->title && currSpecialty2->amount > 0) {
						if (currSpecialty2->amount >= currSpecialty->amount) {
							specialist *currSpecialist = currSpecialty2->specialists;
							specialist *prevSpecialist;
							for (int i = 0; i < currSpecialty2->amount; i++) {
								if (currSpecialist == currSpecialty2->specialists) // if curr == first
									currSpecialty2->specialists = currSpecialty2->specialists->next;
								else
									prevSpecialist->next = currSpecialist->next;
								delete currSpecialist;
								currSpecialist = currSpecialist->next;
							}
							// atimtis baigta
							currSpecialty->amount -= currSpecialty2->amount;
						}
						
					}
					currSpecialty2 = currSpecialty2->next;
				}
				currSpecialty = currSpecialty->next;
			}
			return;
		}
		currCompany = currCompany->next;
	}
	cout << "Ši įmonė neegzistuoja.\n";
}

int main() {
	int opt;
	do {
		cout << "\n1. Registruoti specialybę.\n2. Registruoti specialistą.\n3. Išvesti visus specialistus.\n4. Registruoti įmonę.\n5. Papildyti įmonės specialybių sąrašą.\n6. Išvesti įmonės reikalavimus.\n7. Aprūpinti įmonę specialistais.\n8. Išjungti.\nPasirinkimas: ";
		cin >> opt;
		switch (opt) {
			case 1:
				registerSpecialty();
				break;
			case 2:
				registerSpecialist();
				break;
			case 3:
				listEverything();
				break;
			case 4:
				registerCompany();
				break;
			case 5:
				requestSpecialists();
				break;
			case 6:
				listCompanyRequirements();
				break;
			case 7:
				provideSpecialists();
				break;
			case 8:
				break;
			default:
				cout << "Operation does not exist.\n";
		}
	} while (opt != 8);
	return 0;
}