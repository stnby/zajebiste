# Lietuviškai
Darbo biržoje sudarytas specialybių sąrašas, kurių kiekviena turi atskirą specialistų, turinčių šią specialybę sąrašą (pavardės surūšiuotos pagal abėcėlę). Taip pat formuojamas įmonių, pageidaujančių specialistų sąrašas, kurių kiekviena turi papildomą specialybių su nurodytu pageidaujamų specialybių kiekiu.

Turi būti galimybė:
1. Papildyti specialybių sąrašą;
2. Papildyti nurodytos specialybės specialistų sąrašą;
3. Išvesti visų specialybių sąrašą, su visais specialistais;
4. Papildyti įmonių sąrašą, o taip pat nurodytos įmonės specialybių sąrašą;
5. Išvesti įmonių sąrašą;
6. Išvesti nurodytos firmos reikalaujamų specialybių sąrašą ir kiekinį poreikį ;
7. Aprūpinti nurodytą firmą specialistais pagal jos pageidavimą, atitinkamai pakoreguojant specialybių sąrašo elementų sudėtį (pašalinant iš jos įsidarbinusius specialistus) bei įmonių sąrašą (jei įmonės pageidavimai pilnai patenkinami, ji iš sąrašo pašalinama). 

Visus rūšiavimus atlikti įterpimo metu. Papildomiems rūšiavimams (arba klasifikavimui) realizuoti reikia panaudoti papildomas adresų grandinėles, neformuojant sąrašų, dubliuojančių vienas kitą. 

# English
The labor center has a list of specialties, each of which has a separate list of specialists who have this specialty (alphabetically sorted names). There is also a list of companies that wish to employ a specialist, each of which has an additional specialty with a specified number of required specialties.

It must be possible:
1. To add new specialty into a list of specialties;
2. To add a new specialist to the list of specialty indicated;
3. To print a list of all specialties, with all its specialists;
4. To supplement the list of companies, as well as the list of specialties indicated by the company;
5. Print the list of companies;
6. Print the list of required specialties and the quantity of each company;
7. Provide the firm with the specified specialists on request, adjusting accordingly the composition of the list of specialty items (removing specialists who have been employed by it) and the list of companies (if the company's requests are fully satisfied, it should be removed from the list).

Do all sorting during the insertion. For additional sorting, additional link chains need to be used without creating lists that duplicates each other.
